import csv
import sys

#Constants
TO_DO = 0
IN_PROCESS = 1
TO_VERIFY = 2
DONE = 3

#Created as an interface to managing all the stories
class StoryManager:

    def __init__(self):
        self.story_list = []

    def create_story(self, story_id, description):
        if self.find_story(story_id):
            print "ERROR: Story %s already exists!"%story_id
            return None
        else:
            self.story_list.append(Story(story_id, description))
            return self.story_list[-1]

    def list_stories(self):
        if len(self.story_list) < 1:
            print "There are no stories"
        for i in self.story_list:
            print i.story_id, "/", i.description

    def delete_story(self, story_id):
        to_delete = self.check_exists(story_id)
   
        if to_delete:
            self.story_list.remove(to_delete)
        else:
            print "ERROR: Story %s does not exist!"%story_id
        return

    def complete_story(self, story_id):
        to_complete = self.check_exists(story_id)
        if to_complete:
            to_complete.complete_story()

    def create_task(self, story_id, task_id, description):
        to_create_for = self.check_exists(story_id)
        if to_create_for:
            to_create_for.create_task(task_id, description)

    def list_tasks(self, story_id):
        to_list = self.check_exists(story_id)
        if to_list:
            to_list.list_tasks()

    def delete_task(self, story_id, task_id):
        to_delete_for = self.check_exists(story_id)
        if to_delete_for:
            to_delete_for.delete_task(task_id)

    def move_task(self, story_id, task_id, new_column):
        to_move_for = self.check_exists(story_id)
        if to_move_for:
            to_move_for.move_task(task_id, new_column)

    def update_task(self, story_id, task_id, description):
        to_update_for = self.check_exists(story_id)
        if to_update_for:
            to_update_for.update_task(task_id, description)
    
    def check_exists(self, story_id):
        to_check = self.find_story(story_id)
        if to_check:
            return to_check
        else:
            print "ERROR: Story %s does not exist!"%story_id
            return False
    
    def find_story(self, story_id):
        for i in self.story_list:
            if story_id == i.story_id:
                return i
        return None

    def save_state(self, outfile):
        outstring = ""
        for i in self.story_list:
            outstring += str(i) + "\n"
            for j in i.task_list:
                outstring += str(j) + "\n"

        text_file = open(outfile, "w")
        text_file.write(outstring)
        text_file.close()

    def load_state(self, infile):
        currStory = None #current story
        currStoryComplete = False #Track if story is complete for adding at end of story processing
        with open(infile) as f:
            content = f.read().splitlines()
            for i in content:
                contSplit = csv.reader([i], delimiter = "\t").next()
                if contSplit[0] == "Story: ":
                    if currStory is not None:
                        if currStoryComplete:
                            currStory.complete_story()
                    currStory = self.create_story(contSplit[1], contSplit[2])
                    currStoryComplete = contSplit[3] == 'True'
                elif contSplit[0] == "Task: ":
                    currTask = currStory.create_task(contSplit[1], contSplit[2])
                    currTask.parse_column(int(contSplit[3]))
                

class Story:
    
    def __init__(self, story_id, description):
        self.story_id = story_id
        self.description = description
        self.task_list = []
        self.completed = False

    def __str__(self):
        return "Story: " + "\t" + str(self.story_id) + "\t" + str(self.description) + "\t" + str(self.completed)

    #Will print each task's id, description and column
    def list_tasks(self):
        if len(self.task_list) == 0:
            print "There are no tasks associated with this story."
        
        for i in self.task_list:
            task_column = {TO_DO : 'To Do',
                           IN_PROCESS : 'In Process',
                           TO_VERIFY : 'To Verify',
                           DONE : 'Done'}[i.column]
            print i.task_id, "/", i.description, "/", task_column

    #Checks if all tasks are marked "Done", and if so,
    #marks the story "Complete". If not, the first task
    #seen that is not complete is printed
    def complete_story(self):
        for i in self.task_list:
            if i.column < 3:
                print "ERROR: Task %s is not yet complete!"%i.task_id
                return

        self.completed = True

    def create_task(self, task_id, description):
        if not self.check_completed():
            if self.find_task(task_id):
                print "ERROR: Task %s already exists!"%task_id
                return
            
            self.task_list.append(Task(task_id, description))
            return self.task_list[-1]

    def delete_task(self, task_id):
        to_delete = self.check_exists(task_id)
        if to_delete:
            self.task_list.remove(to_delete)
        return

    #First find the task and if it exists, update it
    def update_task(self, task_id, description):
        to_update = self.check_exists(task_id)
        if to_update:
            to_update.update_task(description)
        return

    #First find the task and if it exists, move it
    def move_task(self, task_id, new_column):
        to_update = self.check_exists(task_id)
        if to_update:
            to_update.move_task(new_column)

    #Find a task by task_id if it exists. Return None otherwise
    def find_task(self, task_id):
        for i in self.task_list:
            if task_id == i.task_id:
                return i
        return None

    def check_exists(self, task_id):
        to_check = self.find_task(task_id)
        if to_check:
            return to_check
        else:
            print "ERROR: task %s does not exist!"%task_id
            return False

    def check_completed(self):
        if self.completed:
            print "This story has been completed and cannot be expanded"
            return True
        else:
            return False

class Task:
    
    def __init__(self, task_id, description):  
        self.task_id = task_id
        self.description = description
        self.column = TO_DO

    def __str__(self):
        return "Task: " + "\t" + str(self.task_id) + "\t" + str(self.description) + "\t" + str(self.column)

    #For manually adding the column
    def parse_column(self, column):
        self.column = column
    
    def move_task(self, new_column):
        #convert the input string into the appropriate number
        new_column = {"To Do" : 0,
                      "In Process" : 1,
                      "To Verify" : 2,
                      "Done" : 3}[new_column]
        
        #If the task has been finished, it cannot be moved
        if self.column == DONE:
            print "ERROR: The task has already been finished and cannot be moved"

        #The user may have attempted to move the task to the column
        #it is already in by accident, so we alert the user of this
        elif self.column == new_column:
            print "ERROR: The task is already in that column!"

        #If the task is being moved too far forward
        elif (new_column - self.column) > 1:
            print "ERROR: Tasks can only be moved forward one column at a time!"

        #If the user's attempt to shift is valid
        else:
            self.column = new_column

        return

    def update_task(self, new_description):
        self.description = new_description

#This is made its own method to make unit testing easier
def get_input():
    return raw_input("Please enter a command: ")

def get_args():
    userin = get_input()
    if userin == "":
        return ["no command"] #Will be read as incorrect command by main
    command_list = csv.reader([userin], delimiter = " ").next()
    command_list = filter(None, command_list) #Remove empty strings
    if command_list[0] == 'quit':
        sys.exit(0)

    return command_list

if __name__ == "__main__":
    manager = StoryManager()
               
    while (True):

        comlist = get_args()
        if (comlist == False):
            continue
        
        if comlist[0] == "create story":
            if len(comlist) == 3:
                manager.create_story(comlist[1], comlist[2])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "list stories":
            if len(comlist) == 1:
                manager.list_stories()
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "delete story":
            if len(comlist) == 2:
                manager.delete_story(comlist[1])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "complete story":
            if len(comlist) == 2:
                manager.complete_story(comlist[1])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "create task":
            if len(comlist) == 4:
                manager.create_task(comlist[1], comlist[2], comlist[3])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "list tasks":
            if len(comlist) == 2:
                manager.list_tasks(comlist[1])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "delete task":
            if len(comlist) == 3:
                manager.delete_task(comlist[1], comlist[2])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "move task":
            if len(comlist) == 4:
                manager.move_task(comlist[1], comlist[2], comlist[3])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "update task":
            if len(comlist) == 4:
                manager.move_task(comlist[1], comlist[2], comlist[3])
                continue
            else:
                print "Incorrect number of arguments"
        elif comlist[0] == "save state":
            if len(comlist) == 2:
                manager.save_state(comlist[1])
                continue
        elif comlist[0] == "load state":
            if len(comlist) == 2:
                manager.load_state(comlist[1])
                continue
        else:
            print "That was not a valid command"
        

        

