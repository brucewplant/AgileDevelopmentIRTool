import unittest
import sys
import iragile as ir

class TaskTest(unittest.TestCase):

    def setUp(self):
        self.test_task= ir.Task("test_task", "task_for_testing")

    def test_task_column_can_be_incremented(self):
        self.test_task.move_task("In Process")
        self.assertEquals(1, self.test_task.column)

    def test_task_column_can_be_decremented(self):
        self.test_task.move_task("In Process")
        self.test_task.move_task("To Do")
        self.assertEquals(0, self.test_task.column)

    def test_task_column_can_be_decremented_twice(self):
        self.test_task.move_task("In Process")
        self.test_task.move_task("To Verify")
        self.test_task.move_task("In Process")
        self.test_task.move_task("To Do")
        self.assertEquals(0, self.test_task.column)

    def test_task_column_can_be_set_to_done(self):
        self.test_task.move_task("In Process")
        self.test_task.move_task("To Verify")
        self.test_task.move_task("Done")
        self.assertEquals(3, self.test_task.column)

    def test_task_description_can_be_updated(self):
        self.test_task.update_task("This is the new description")
        self.assertEquals("This is the new description",
                          self.test_task.description)

class StoryTest(unittest.TestCase):

    def setUp(self):
        self.test_story = ir.Story("test story", "story for testing")

    def test_creating_one_task(self):
        self.test_story.create_task("test task", "test description")
        self.assertEquals(1, len(self.test_story.task_list))

    def test_deleting_one_task(self):
        self.test_story.create_task("test task", "test description")
        self.test_story.delete_task("test task")
        self.assertEquals(0, len(self.test_story.task_list))

    def test_update_one_task(self):
        self.test_story.create_task("test task", "test description")
        self.test_story.update_task("test task", "updated description")
        self.assertEquals("updated description",
                          self.test_story.task_list[0].description)

    def test_move_one_task(self):
        self.test_story.create_task("test task", "test description")
        self.test_story.move_task("test task", "In Process")
        self.assertEquals(1, self.test_story.task_list[0].column)

    def test_complete_story(self):
        self.test_story.complete_story()
        self.assertEquals(True, self.test_story.completed)

class StoryManagerTest(unittest.TestCase):

    def setUp(self):
        self.test_story_manager = ir.StoryManager()

    def test_creating_two_stories(self):
        self.test_story_manager.create_story("story test", "desc")
        self.test_story_manager.create_story("test story", "desc")
        self.assertEquals(2, len(self.test_story_manager.story_list))

    def test_deleting_story(self):
        self.test_story_manager.create_story("story test", "desc")
        self.test_story_manager.delete_story("story test")
        self.assertEquals(0, len(self.test_story_manager.story_list))

    def test_completing_story(self):
        self.test_story_manager.create_story("story test", "desc")
        self.test_story_manager.complete_story("story test")
        self.assertEquals(True, self.test_story_manager.story_list[0].completed)
        
if __name__ == '__main__':
    try: unittest.main()
    except SystemExit: pass
